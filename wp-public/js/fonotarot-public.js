(function( $ ) {
	'use strict';

	/**
	 * All of the code for your public-facing JavaScript source
	 * should reside in this file.
	 *
	 * Note: It has been assumed you will write jQuery code here, so the
	 * $ function reference has been prepared for usage within the scope
	 * of this function.
	 *
	 * This enables you to define handlers, for when the DOM is ready:
	 *
	 * $(function() {
	 *
	 * });
	 *
	 * When the window is loaded:
	 *
	 * $( window ).load(function() {
	 *
	 * });
	 *
	 * ...and/or other possibilities.
	 *
	 * Ideally, it is not considered best practise to attach more than a
	 * single DOM-ready or window-load handler for a particular page.
	 * Although scripts in the WordPress core, Plugins and Themes may be
	 * practising this, we should strive to set a better example in our own work.
	 */

})( jQuery );



function init_fonotarot(wrapper, url) {
	console.log("Iniciando Plugin Fonotarot en #"+ wrapper.id);
	start_fonotarot(wrapper, url);
  }
  
  function start_fonotarot(wrapper, url) {
	console.log("Obteniendo "+ url);
	var da_html = "";
	jQuery.getJSON(url).done(function(json_payload) { 
	  
	  if (json_payload.length == 0) return;
	  var lista = [];
	  jQuery.each(json_payload, function(key, val) {
		lista.push(generate_card(val));
	  });
	  da_html = lista.join("");
	  
	  wrapper.innerHTML = da_html;
	});
  
	setTimeout(start_fonotarot, 10000, wrapper, url);
  
  }
  
  function generate_card(agente) {
	console.log(agente);
	var btn_color = "primary";
	var btn_text = "Sin Información";
	if ((agente.ingreso == 1) && (agente.disponible == 1)) {
	  btn_color = "success";
	  btn_text = "Llama! Opción " + agente.opcion;
	} else if ((agente.ingreso == 1) && (agente.disponible == 0)) {
	  btn_color = "warning";
	  btn_text = "En una llamada";
	} else {
	  btn_color = "danger";
	  btn_text = "No Disponible";
	}
	var html_card = `
	<div class="col-sm-12">
	  <div class="card mb-3" style="max-width: 540px;">
		<div class="row">
		  <div class="col-md-12 col-sm-12">
			<div class="card-header text-capitalize">
			${agente.nombre}
			<a href="tel:+56222301515" class="btn btn-sm btn-${btn_color} pull-right">${btn_text}</a>
			</div>          
		  </div>
		</div>
	  </div>
	</div>
	`;
	return html_card;
  }