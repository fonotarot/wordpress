<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       https://about.me/mariofix
 * @since      1.0.1
 *
 * @package    Fonotarot
 * @subpackage Fonotarot/wp-public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Fonotarot
 * @subpackage Fonotarot/wp-public
 * @author     mariofix <yo@mariofix.com>
 */
class Fonotarot_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.1
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.1
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.1
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.1
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Test_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Test_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/fonotarot-public.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.1
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Test_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Test_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/fonotarot-public.js', array( 'jquery' ), $this->version, false );

	}

	public function fonotarot_lista_ejecutivos( $atts ) {
    
		// Extract the shortcode attributes
		$atts = shortcode_atts( array(
			'url' => 'https://firenze.156.cl/audiotex/ejecutivos',
			'timeout' => 10
		), $atts );
		
		$fonotarot_wrapper = '<div class="row col–sm-4 overflow-y-auto" id="fonotarot-wrapper"><p class="alert alert-warning">Intenta más tarde para encontrar tarotistas en línea.</p></div><script>if (typeof init_fonotarot === "function") {init_fonotarot(document.getElementById("fonotarot-wrapper"), "'.$atts["url"].'");} else { alert("Ocurrio un error al cargar la lista de ejecutivos."); }</script>';
		
		// Set the final output
		$output = '<div id="grabby-wrapper">' . $fonotarot_wrapper . '</div>';
		
		return $output;
	}
}




