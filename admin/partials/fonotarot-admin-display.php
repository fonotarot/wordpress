<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       https://about.me/mariofix
 * @since      1.0.1
 *
 * @package    Fonotarot
 * @subpackage Fonotarot/admin/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
