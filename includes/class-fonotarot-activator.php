<?php

/**
 * Fired during plugin activation
 *
 * @link       https://about.me/mariofix
 * @since      1.0.1
 *
 * @package    Fonotarot
 * @subpackage Fonotarot/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.1
 * @package    Fonotarot
 * @subpackage Fonotarot/includes
 * @author     mariofix <yo@mariofix.com>
 */
class Fonotarot_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.1
	 */
	public static function activate() {

	}

}
