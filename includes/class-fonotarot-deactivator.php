<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://about.me/mariofix
 * @since      1.0.1
 *
 * @package    Fonotarot
 * @subpackage Fonotarot/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.1
 * @package    Fonotarot
 * @subpackage Fonotarot/includes
 * @author     mariofix <yo@mariofix.com>
 */
class Fonotarot_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.1
	 */
	public static function deactivate() {

	}

}
