# Fonotarot
Muestra lista de ejecutivos del sistema y su estado

# Instalacion
- Descargar zip de release desde https://gitlab.com/fonotarot/wordpress/-/archive/fonotarot/wordpress-fonotarot.zip
- Ir a Plugins -> Agregar Nuevo - Cargar Archivo y subir el wordpress-fonotarot.zip.
- Activar plugin y agregarlo al contenido con *[fonotarot]*
- La URl puede ser modificada con *[fonotarot url="Nueva url"]*

# Ejemplo
![Lista Ejecutivos](fonotarot-plugin-1.webp)*ejemplo en fonotarot.com*
