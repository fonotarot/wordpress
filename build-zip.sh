#!/bin/bash
rm -v fonotarot.zip
mkdir -v fonotarot
cp -vr admin fonotarot
cp -vr includes fonotarot
cp -vr languages fonotarot
cp -vr wp-public fonotarot
cp -v fonotarot.php fonotarot
cp -v index.php fonotarot
cp -v uninstall.php fonotarot
cp -v LICENSE.txt fonotarot
cp -v README.md fonotarot
cp -v README.txt fonotarot
zip -r fonotarot.zip fonotarot/
rm -rfv fonotarot/