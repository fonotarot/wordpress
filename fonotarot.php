<?php

/**
 * Fonotarot
 *
 * Shortcodes y widgets para operar con fonotarot.com
 *
 * @link              https://about.me/mariofix
 * @since             1.0.0
 * @package           Fonotarot
 *
 * @wordpress-plugin
 * Plugin Name:       Fonotarot
 * Plugin URI:        https://fonotarot.gitlab.io/wordpress/
 * Update URI:		  https://fonotarot.gitlab.io/wordpress/plugin-version.json
 * Description:       Shortcodes y widgets para operar con fonotarot.com
 * Version:           1.0.1
 * Requires at least: 6.0
 * Requires PHP:	  7.4
 * Author:            Fonotarot
 * Author URI:        https://fonotarot.com
 * License:           MIT
 * Text Domain:       fonotarot
 * Domain Path:       /languages
 
 * 
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'FONOTAROT_VERSION', '1.0.1' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-fonotarot-activator.php
 */
function activate_fonotarot() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-fonotarot-activator.php';
	Fonotarot_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-fonotarot-deactivator.php
 */
function deactivate_fonotarot() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-fonotarot-deactivator.php';
	Fonotarot_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_fonotarot' );
register_deactivation_hook( __FILE__, 'deactivate_fonotarot' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-fonotarot.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.1
 */
function run_fonotarot() {

	$plugin = new Fonotarot();
	$plugin->run();

}
run_fonotarot();
