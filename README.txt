=== Plugin Name ===
Contributors: mariofix
Tags: fonotarot, tarot, astrologia, chile, cartas, marsella
Requires at least: 6.0
Tested up to: 6.2
Stable tag: trunk
Requires PHP: 7.4
License: MIT

Shortcodes y widgets para operar con fonotarot.com

== Description ==

Shortcodes y widgets para operar con fonotarot.com

== Installation ==

Download https://fonotarot.gitlab.io/wordpress/fonotarot.zip and install it via the admin panel.
User shortcode [fontarot] to get the list.

== Frequently Asked Questions ==

= ¿Necesito algun dato especial para utilizar este plugin? =

Este plugin está pensado para empresas que deseen utilizar los servicios de Fonotarot. No está pensado para el publico general.

== Screenshots ==



== Changelog ==

= 1.0.0 =
* First Version

= 1.0.1 =
* New Structura thanks to wppb.me